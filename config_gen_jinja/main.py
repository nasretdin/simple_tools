import jinja2
from jinja2 import FileSystemLoader, Environment

src_net_addr = '192.168.100.0'
dst_net = '192.168.1.0'
peer = '172.16.1.2'

def main():
    templateLoader = FileSystemLoader(searchpath="./templates/")
    templateEnv = Environment(loader=templateLoader)
    TEMPLATE_FILE = "template.txt"
    template = templateEnv.get_template(TEMPLATE_FILE)
    ports1 = list(range(10000, 11000))

    result = template.render(ports=ports1,
                             src_net_addr=src_net_addr,
                             dst_net=dst_net,
                             peer=peer)

    with open('output.txt', 'w') as output:
        output.write(result)


if __name__ == '__main__':
    main()