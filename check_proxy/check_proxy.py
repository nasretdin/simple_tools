import argparse
import sys
import time
from concurrent.futures import ProcessPoolExecutor

import pandas as pd
import requests
from requests import RequestException

http_proxies_url = 'https://free-proxy-list.net/'
ssl_proxies_url = 'https://www.sslproxies.org/'


def progress_indicator(param):
    # print(param.result())
    print('.', end='', flush=True)


def get_proxy_list(url):
    # https://raw.githubusercontent.com/TheSpeedX/SOCKS-List/master/http.txt
    # https://github.com/jetkai/proxy-list/tree/main/online-proxies
    page = requests.get(url)
    proxy_table = pd.read_html(page.text)[0]
    proxies_df = proxy_table[['IP Address', 'Port']]
    proxies_list = []
    for table_row in proxies_df.iterrows():
        addr = table_row[1]['IP Address']
        port = table_row[1]['Port']
        proxies_list.append({'https': f'http://{addr}:{str(port)}'})

    return proxies_list


def check_proxy(proxy):
    try:
        # if google.com available through proxy then this is alive.
        start_time = time.time()
        requests.get('https://google.com/', proxies=proxy, timeout=10, verify=True)
        end_time = time.time() - start_time
    except (RequestException, OSError):
        pass
    else:
        result_str = f"{proxy.get('https').replace('http://', '')}"
        return {'proxy': result_str, 'time': end_time}
    return


def make_proxies_list() -> list:
    http_proxies = get_proxy_list(url=http_proxies_url)
    ssl_proxies = get_proxy_list(url=ssl_proxies_url)
    _proxies = [item for item in ssl_proxies if item not in http_proxies] + http_proxies
    print(f'Total addresses: {len(_proxies)}')
    return _proxies


def make_sorted_proxies_table(proxies_dict: list):
    data = []
    sorted_table = pd.DataFrame(columns=['proxy', 'time'])
    for item in proxies_dict:
        if item is not None:
            tmp_df = pd.DataFrame([item])
            data.append(tmp_df)
            sorted_table = sorted_table.append(tmp_df, ignore_index=True)

    sorted_table = sorted_table.sort_values(by='time')
    return sorted_table


def cmd_args_parse():
    cmd_parser = argparse.ArgumentParser()
    cmd_parser.add_argument('-t', '--timeout', default=10, type=int)
    cmd_parser.add_argument('-w', '--workers', default=60, type=int)
    cmd_parser.add_argument('-q', dest='quiet', default=False, type=bool, action='store', const=True, nargs='?')
    _cmd_args = cmd_parser.parse_args()
    return _cmd_args


def run_proxy_check(proxies: list):
    _proxies_dicts_list = []
    # start the process pool
    print(f'Start check. Thread nums: {max_workers}, tcp_check_timeout: {tcp_timeout} sec.')
    with ProcessPoolExecutor(max_workers=max_workers) as executor:
        # send in the tasks
        futures = [executor.submit(check_proxy, i) for i in proxies]
        # register the progress indicator  function as callback
        for future in futures:
            future.add_done_callback(progress_indicator)
            # get results from future objects
            proxy_dict = future.result()
            if proxy_dict is not None:
                _proxies_dicts_list.append(proxy_dict)
    return _proxies_dicts_list


if __name__ == '__main__':

    cmd_args = cmd_args_parse()
    tcp_timeout = cmd_args.timeout
    max_workers = cmd_args.workers
    quiet = cmd_args.quiet

    # Do full proxy addresses list
    proxies_list = make_proxies_list()

    # Do alive proxies list
    proxies_dicts_list = run_proxy_check(proxies_list)

    # Create sorted table of alive proxies
    results = make_sorted_proxies_table(proxies_dicts_list)

    print('\nAlive proxies:')
    if quiet:
        print(results['proxy'].to_csv(index=False))
    else:
        print(results)
    sys.exit(0)
