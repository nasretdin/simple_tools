"""
Необходимо реализовать алгоритм поиска корня из натурального числа с заданной точностью.
На входе 2 параметра N- число и e- точность, с которой необходимо вычислить корень.

Пример: my_sqrt(2, 0.01) возвращает 1.41
"""
from argparse import ArgumentParser


class CmdParser(ArgumentParser):
    def error(self, message):
        self.print_help()
        exit(1)


def calc_sqrt(input_num: int, e: int) -> str:
    def get_facets(facet_value: int) -> list:
        tmp_str = str(facet_value)
        if len(tmp_str) % 2 != 0:
            tmp_str = f'0{tmp_str}'
        tmp_str = list(tmp_str)
        facets_list = []
        while tmp_str[:]:
            facets_list.insert(0, ''.join(tmp_str[-2:]))
            for _ in range(2):
                tmp_str.pop(len(tmp_str) - 1)
        return facets_list

    def get_next_num(last_result: int, input_facet: str) -> (str, str):
        start_value = int(f'{last_result}{1}')
        for j in range(0, 10):
            next_num = int(f'{last_result}{j}') * j
            control = int(input_facet) - next_num
            if control > start_value:
                continue
            else:
                return str(j), str(next_num)

    sqrt_result = ''
    facets = get_facets(input_num)
    int_part_num = len(facets)
    for _ in range(e):
        facets.append('00')
    remnant = ''
    double_result = ''
    for k, facet in enumerate(facets):
        facet = remnant + facet
        if k == 0:
            root_item = str(int(facet)**0.5).split('.')[0]
            remnant = str(int(facet) - int(root_item) ** 2)
        else:
            root_item, subtrahend = get_next_num(double_result, facet)
            remnant = str(int(facet) - int(subtrahend))
        sqrt_result += str(root_item)
        double_result = int(sqrt_result) * 2
    sqrt_result = sqrt_result[:int_part_num] + ',' + sqrt_result[int_part_num:]
    return sqrt_result


def get_sqrt(num: int = 0, e: float = 0.1) -> float:
    counter = 0
    while e < 1:
        e = e * 10
        counter += 1
    return float(round(num ** 0.5, counter))


if __name__ == '__main__':
    # ========================= For reference ================================
    # =========== https://www.gutenberg.org/files/129/129.txt ================
    # =========== The Square Root of Two, to 5 million digits ================
    help_text = 'You need enter: positive integer and positive float or integer. ' \
                'Example: python3 main.py -n 2 -a 0.01 or python3 main.py -n 2 -a 20'
    cmd_parser = CmdParser(description=help_text)
    cmd_parser.add_argument('-n', '--number', required=True, type=int, help='Integer.')
    accuracy = cmd_parser.add_mutually_exclusive_group()

    accuracy.add_argument('-af', '--accuracy_float', type=float, help='Accuracy float.')
    accuracy.add_argument('-an', '--accuracy_int',  type=int, help='Accuracy integer.')
    cmd_args = cmd_parser.parse_args()
    number = cmd_args.number
    accuracy_float = cmd_args.accuracy_float
    accuracy_int = cmd_args.accuracy_int

    if accuracy_int:
        square_root = calc_sqrt(number, accuracy_int)
        print('The square root of {0} is {1}'.format(number, square_root))
    elif accuracy_float:
        sqrt = get_sqrt(number, accuracy_float)
        print('The square root of {0} is {1}'.format(number, sqrt))
    else:
        cmd_parser.print_help()


